# Description 
Implement a microservice which provides a list of countries and, in addition, provides more detailed information per country.

Implementation using JHipster as template for microservice architecture (https://www.jhipster.tech/)

# Install 
- AdoptOpenJDK Java 11 (LTS Build)
- Maven 3.6
- Node (LTS)
- Git

# Tech stack 
- Microservice architecture
    - Service Registry
    - API gateway 
        - LB Ribbon 
        - Zuul 
    - Circuit breaker Hystrix 
    - Spring security 
- Spring boot 
- Reactor core
- Maven 
- JHipster
- API doc : Swagger 
    - API endpoint `http://localhost:8081/v2/api-docs`
- Unit test 
- Integration test 


# How to run locally 
- Start Service Registry
    - cd jhipster-registry
    - `./mvnw`    
    - goto `http://localhost:8761/`
- Start `gateway`
    - `cd gateway`
    - `./mvnw`   
    (config your MySQL Db locally with user : root, pass: password) 
- Start `countryservice`
    - `cd countryservice`
    - `./mvnw`    
- Open `http://localhost:8080/`
- Sign in : admin/admin (user/pass)
- Under entities: click Country
- View will fetch detailed country

# Run test 
- cd `jhipster-registry`/ `gateway` / `countryservice`
- `./mvnw clean test` : Unit test backend
- `./mvnw clean verify` : Integration test backend 

